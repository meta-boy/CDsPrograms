// Evaluation of postfix expression using stack(EXP-3)
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#define max_size 20
int stack[max_size], top = -1;
void push(int ch);
int pop();
void main()
{
    char postfix[max_size];
    int i, op1, op2;
    printf("Enter a postfix expression to evaluate: ");
    scanf("%s", postfix);
    for (i = 0; postfix[i] != '\0'; i++)
    {
        if (isdigit(postfix[i]))
        {
            push(postfix[i] - '0'); // - '0' converts to integer
        }
        else
        {
            // Store operands and perform operation according to the operator
            op2 = pop();
            op1 = pop();
            switch (postfix[i])
            {
            case '+':
                push(op1 + op2);
                break;
            case '-':
                push(op1 - op2);
                break;
            case '*':
                push(op1 * op2);
                break;
            case '/':
                push(op1 / op2);
                break;
            case '^':
                push(pow(op1, op2));
                break;
            default:
                printf("\nError! Invalid operator %c\n", postfix[i]);
            }
        }
    }
    //store the value on top of stack and pop it
    int value = stack[top--];
    //check if stack empty
    if (top != -1)
    {
        printf("Invalid Expression\n");
    }
    else
        printf("\nThe evaluated value is : %d\n", value);
}
void push(int ch)
{
    stack[++top] = ch;
}
int pop()
{
    int op = stack[top--];
    return op;
}

/*
// source@beth:~/Desktop/UNI/DS$ gcc postfix-eval.c -lm -o
postfix-eval && ./postfix-eval
// Enter a postfix expression to evaluate: 6523+8*+3+*
// The evaluated value is : 288
// source@beth:~/Desktop/UNI/DS$ gcc postfix-eval.c -lm -o
postfix-eval && ./postfix-eval
// Enter a postfix expression to evaluate: 6523+8*+3+
// Invalid Expression
*/