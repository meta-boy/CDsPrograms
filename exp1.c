//program for array implementation of Stack(EXP-1) #include<stdio.h>
#include <stdlib.h>
#include <string.h>
#define max_size

int stack[max_size];
int bottom = 0, top = -1;
//function prototypes void push(char c); char pop();
void display();
int palindromeCheck();

void main()
{
    int op, element;
    char ch;
    do
    {
        printf("\n--------------Enter an operation to perform------------");
        printf("\n1.Push\n2.Pop\n3.Display\n4.Check string for palindrome\
n5.Exit\n");
        printf("Enter choice: ");
        scanf("%d", &op);
        switch (op)
        {
        case 1:
            printf("\nEnter element to push: ");
            scanf("%d", &element);
            push(element);
            break;
        case 2:
            pop();
            break;
        case 3:
            display();
            break;
        case 4:
            palindromeCheck(element);
            break;
        case 5:
            exit(0);

        default:
            printf("\nENTER CORRECT OPTION (1-8)!");
        }
        printf("\nContinue? (y/n): ");
        ch = getchar();
        ch = getchar();
        printf("\n");
    } while (ch == 'y' || ch == 'Y');
}

void push(char c)
{
    if (top == max_size - 1)
    {
        printf("Stack Overflow.\n\n");
        exit(1);
    }
    else
        stack[++top] = c;
}
char pop()
{
    if (top == -1)
        printf("\nStack Underflow");
    else
        return stack[top--];
}

void display()
{
    if (top == -1)
        printf("Stack is Empty.\n");
    else
    {
        for (int i = 0; i <= top; i++)
            printf("%d\n", stack[i]);
    }
}

int palindromeCheck(char string[])
{
    //empty stack if it contains elements while(top != -1)
    top--;
    //get string from user char string[10];
    int i;
    printf("\nEnter a string to check if it is palindrome or not: ");
    scanf("%s", string);
    //Push String to Stack int n = strlen(string); for (i = 0; i < n; i++)
    push(string[i]);

    //Iterate through the stack, Pop and check for character equality
    //set flag to 0 if mismatch int flag = 1;
    int flag, n = strlen(string);
    for (i = 0; i < n; i++)
    {
        char popc = pop();
        if (popc != stack[bottom])
        {
            flag = 0;
            break;
        }
        else
            bottom++;
    }
    if (flag)
        printf("\nThe String \"%s\" is a palindrome.\n", string);
    else
        printf("\nThe String \"%s\" is not a palindrome.\n", string);
}

/* ---------------------OUTPUT---------------------
source@beth:~/Desktop/UNI/DS$ gcc palindrome.c -o palindrome && ./palindrome

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 2

Stack Underflow Continue? (y/n): y


--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 1 Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 2 Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 3 Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 5 Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 6 Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 1

Enter element to push: 7 Stack Overflow

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 4

Enter a string to check if it is palindrome or not: NITIN The String "NITIN" is a palindrome.
Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice: 4

Enter a string to check if it is palindrome or not: RAIT The String "RAIT" is not a palindrome.
Continue? (y/n): y

--------------Enter an operation to perform------------ 1.Push
2.Pop 3.Display
4.Check string for palindrome 5.Exit
Enter choice:4

Enter a string to check if it is palindrome or not: ABCCBA StackOverflow.
*/