//Singly Linked list(EXP-5)
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
int size = 0; //variable to account for sll size
struct stu_node
{
    int roll_no, sem;
    float gpa;
    char name[20], branch[5];
    struct stu_node *next;
};
struct stu_node *start = NULL;
struct stu_node *end = NULL;
//function prototypes
void get_data();
bool isEmpty();
void insert_end();
void insert_front();
void insert_middle();
void delete_front();
void delete_end();
void delete_middle();
void del_data();
void display();
void main()
{
    int op;
    char ch;
    struct stu_node *disp;
    do
    {
        printf("\n------------STUDENT DATA RECORD-------------------\n");
        printf("\n1.Insert Student Data(end)\n2.Insert student data(front)\n3.Insert Student Data(middle)\n");
        printf("4.Delete data(end)\n5.Delete data(middle)\n6.Delete data(front)\n7.Display Data\n8.Exit\n-------------------------------------------------\n");
        printf("Enter choice: ");
        scanf("%d", &op);
        switch (op)
        {
        case 1:
            insert_end();
            break;
        case 2:
            insert_front();
            break;
        case 3:
            insert_middle();
            break;
        case 4:
            delete_end();
            break;
        case 5:
            delete_middle();
            break;
        case 6:
            delete_front();
            break;
        case 7:
            display();
            break;
        case 8:
            exit(0);
        default:
            printf("\nENTER CORRECT OPTION (1-8)!");
        }
        printf("\nContinue? (y/n): ");
        ch = getchar();
        ch = getchar();
        printf("\n");
    } while (ch == 'y' || ch == 'Y');
    // getchar();
}
//function to get data on store in a pointer
void get_data(struct stu_node *ptr)
{
    printf("\nEnter Roll Number: ");
    scanf("%d", &ptr->roll_no);
    printf("\nEnter Name: ");
    scanf("%s", ptr->name);
    printf("\nEnter Branch: ");
    scanf("%s", ptr->branch);
    printf("\nEnter semester: ");
    scanf("%d", &ptr->sem);
    printf("\nEnter GPA: ");
    scanf("%f", &ptr->gpa);
    ptr->next = NULL;
}
//function to check if the list is empty
bool isEmpty()
{
    if (start == NULL && end == NULL)
        return true;
    else
        return false;
}
//function to display deleted data
void del_data(struct stu_node *t)
{
    printf("Deleted Data for roll number: %d\n", t->roll_no);
}
void insert_front()
{
    struct stu_node *ptr;
    ptr = (struct stu_node *)malloc(sizeof(struct stu_node));
    get_data(ptr);
    //empty state of list
    if (start == NULL)
        start = ptr;
    //general list state
    else
    {
        ptr->next = start;
        start = ptr;
    }
    size++;
}
//function to insert data at the end of the list
void insert_end()
{
    struct stu_node *ptr;
    ptr = (struct stu_node *)malloc(sizeof(struct stu_node));
    get_data(ptr);
    //empty state
    if (isEmpty())
    {
        start = ptr;
        end = ptr;
    }
    //general state of list
    else
    {
        end->next = ptr; //link
        end = ptr;
    }
    size++;
}
//function to delete data at end of the list
void delete_end()
{
    struct stu_node *t;
    t = start;
    //empty list
    if (isEmpty())
        printf("\nList is Empty!\n");
    //one element in list
    else if (start == end)
    {
        del_data(start);
        start = NULL;
        end = NULL;
    }
    //general state
    else
    {
        while (t->next->next != NULL)
        {
            t = t->next;
        }
        del_data(end);
        end = t;
        end->next = NULL;
    }
    size--;
}
//function to delete data at front
void delete_front()
{
    //empty list
    if (isEmpty())
        printf("\nList is Empty!\n");
    //one element in list
    else if (start == end)
    {
        del_data(start);
        start = NULL;
        end = NULL;
    }
    //general state
    else
    {
        del_data(start);
        start = start->next;
    }
    size--;
}
//function to delete data at middle
void delete_middle()
{
    struct stu_node *t;
    t = start;
    int roll;
    //empty row state
    if (isEmpty())
        printf("\nList is Empty!\n");
    //one element in list
    else if (start == end)
    {
        start = NULL;
        end = NULL;
    }
    else
    {
        printf("\nEnter roll number to delete: ");
        scanf("%d", &roll);
        while (t->next->roll_no != roll)
            t = t->next;
        del_data(t->next);
        t->next = t->next->next;
    }
    size--;
}
//function to insert data at middle
void insert_middle()
{
    //check if list has only one or no element
    if (start == end || start == NULL)
    {
 printf("error! only one element or no element, try Insertion at end or front\n");
    }
    else
    {
        struct stu_node *ptr;
        struct stu_node *t;
        t = start;
        ptr = (struct stu_node *)malloc(sizeof(struct stu_node));
        int index, i = 1;
        printf("\nEnter index of insertion: ");
        scanf("%d", &index);
        get_data(ptr);
        //empty list
        if (isEmpty())
            printf("\nList is Empty!\n");
        //general state
        else
        {
            while (i != index)
            {
                t = t->next;
                i++;
            }
            ptr->next = t->next;
            t->next = ptr;
            size++;
        }
    }
}
// function to display data in the list
void display()
{
    struct stu_node *t;
    if (isEmpty())
    {
        printf("\nEmpty List!\n");
        return;
    }
    t = start;
    printf("\nNAME\t ROLL-N BRANCH\t SEM\t GPA\n\n");
    while (t->next != NULL)
    {
        printf("%s\t %d\t %s\t %d\t %.2f\t \n", t->name, t->roll_no, t->branch,
               t->sem, t->gpa);
        t = t->next;
    }
    printf("%s\t %d\t %s\t %d\t %.2f\t \n", t->name, t->roll_no, t->branch, t -> sem, t->gpa);
}
/*----------------------------OUTPUT--------------------------------
 source@beth:~/Desktop/UNI/DS$ ./linkedlist
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 1
Enter Roll Number: 1
Enter Name: john
Enter Branch: ce
Enter semester: 1
Enter GPA: 6
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 1
Enter Roll Number: 2
Enter Name: cena
Enter Branch: ce
Enter semester: 2
Enter GPA: 8
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 2
Enter Roll Number: 3
Enter Name: frank
Enter Branch: it
Enter semester: 3
Enter GPA: 8
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
NAME ROLL-N BRANCH SEM GPA
frank 3 it 3 8.00
john 1 ce 1 6.00
cena 2 ce 2 8.00 
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 3
Enter index of insertion: 2
Enter Roll Number: 4
Enter Name: ben
Enter Branch: ce
Enter semester: 2
Enter GPA: 7
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
NAME ROLL-N BRANCH SEM GPA
frank 3 it 3 8.00
john 1 ce 1 6.00
ben 4 ce 2 7.00
cena 2 ce 2 8.00
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 4
Deleted Data for roll number: 2
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
NAME ROLL-N BRANCH SEM GPA
frank 3 it 3 8.00
john 1 ce 1 6.00
ben 4 ce 2 7.00
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 6
Deleted Data for roll number: 3
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
NAME ROLL-N BRANCH SEM GPA
john 1 ce 1 6.00
ben 4 ce 2 7.00 
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 5
Enter roll number to delete: 4
Deleted Data for roll number: 4
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
NAME ROLL-N BRANCH SEM GPA
john 1 ce 1 6.00
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 4
Deleted Data for roll number: 1
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 4
List is Empty!
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 5
List is Empty!
Continue? (y/n): y
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 6
List is Empty!
Continue? (y/n):
------------STUDENT DATA RECORD-------------------
1.Insert Student Data(end)
2.Insert student data(front)
3.Insert Student Data(middle)
4.Delete data(end)
5.Delete data(middle)
6.Delete data(front)
7.Display Data
8.Exit
-------------------------------------------------
Enter choice: 7
Empty List!
Continue? (y/n): n
*/