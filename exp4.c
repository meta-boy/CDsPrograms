//Program for array implementation of cicular queue(EXP-4)
#include <stdio.h>
#include <stdlib.h>
#define size 5
int cq[10], front = -1, rear = -1;
void insert();
void delete ();
void display();
void main()
{
    int ch;
    char ch1;
    do
    {
        printf("Enter your choice for circular queue:\n1.Insertion\
n2.Deletion\n3.Display\n4.Exit\n");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
            insert();
            break;
        case 2:
            delete ();
            break;
        case 3:
            display();
            break;
        case 4:
            exit(0);
        default:
            printf("Enter choice (1-4)\n");
        }
        printf("Do you want to continue(y/n)? : ");
        ch1 = getchar(); //memory buffer workaround
        ch1 = getchar();
    } while (ch1 == 'y' || ch1 == 'Y');
}
void insert()
{
    int x;
    if ((rear + 1) % size == front)
        printf("\nCircular Queue Overflow.\n");
    //empty queue state
    else if (front == -1 && rear == -1)
    {
        front = rear = 0;
        printf("\nEnter element for insertion: ");
        scanf("%d", &x);
        cq[rear] = x;
    }
    //general queue state
    else
    {
        rear = (rear + 1) % size;
        printf("\nEnter element for insertion: ");
        scanf("%d", &x);
        cq[rear] = x;
    }
}
void delete ()
{
    //empty queue state
    if (front == -1 && rear == -1)
        printf("\nCircular Queue Underflow.\n\n");
    //single element in queue
    else if (front == rear)
    {
        printf("\n Deleted Element %d\n", cq[rear]);
        front = rear = -1;
    }
    //general queue state
    else
    {
        front = (front + 1) % size;
    }
}
void display()
{
    int i;
    if (rear < front)
    {
        for (i = front; i <= size - 1; i++)
            printf("%d\t", cq[i]);
        for (i = 0; i < front; i++)
            printf("%d\t", cq[i]);
    }
    else
    {
        for (i = front; i <= rear; i++)
            printf("%d\t", cq[i]);
    }
}
/*
---------------------OUTPUT--------------
source@beth:~/Desktop/UNI/DS$ gcc cq.c -o cq && ./cq
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
2
Circular Queue Underflow.
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 1
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 2
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 3
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 4
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 5
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Circular Queue Overflow.
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
2
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
3
2 3 4 5 Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
2
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
3
3 4 5 Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 6
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
1
Enter element for insertion: 7
Do you want to continue(y/n)? : y
Enter your choice for circular queue:
1.Insertion
2.Deletion
3.Display
4.Exit
3
3 4 5 6 7 Do you want to continue(y/n)? : n

*/