

// Program for infix expression to postfix expresssion conversion(EXP-2)
#include <stdio.h>
#include <stdlib.h>
int precedence(char ch);
int isoperand(char ch);
void main()
{
    int i = 0, j = 0, top = -1, operator= 0, operand = 0;
    char infix[20], postfix[20], stack[20];
    printf("Enter infix expression: ");
    scanf("%s", infix);
    // Iterate through the infix characters
    for (i = 0; infix[i] != '\0'; i++)
    {
        // Check for operand
        if (isoperand(infix[i]))
        {
            postfix[j++] = infix[i];
            operand++;
        }
        // Push whatever operator to stack if empty
        else if (top == -1)
        {
            stack[++top] = infix[i];
            operator++;
        }
        // Precedence check for operators
        else if (precedence(infix[i]) > precedence(stack[top]))
        {
            stack[++top] = infix[i];
            operator++;
        }
        else if (precedence(infix[i]) <= precedence(stack[top]))
        {
            if (stack[top] == '(')
            {
                stack[++top] = infix[i];
                operator++;
            }
            else if (infix[i] == ')')
            {
                while (stack[top] != '(') //pop till ‘)’ is found and pop that
                // too without storing
                {
                    postfix[j++] = stack[top--];
                    operator++;
                }
                top--;
            }
            // Pop til operator with lower precedence is found
            else
            {
                while ((precedence(infix[i]) <= precedence(stack[top])) && (top != -1))
                {
                    postfix[j++] = stack[top--];
                    operator++;
                }
                stack[++top] = infix[i];
            }
            // Push when appropriate precedence is found
        }
    }
    // Empty stack when loop terminates
    while (top != -1)
    {
        if (stack[top] == '(' || operator>= operand)
        {
            printf("Invalid Expression\n");
            exit(0);
        }
        else
            postfix[j++] = stack[top--];
        postfix[j] = '\0';
    }
    printf("Postfix Expression: %s\n", postfix);
}
int precedence(char ch)
{
    if (ch == '(')
        return 4;
    if (ch == '^' || ch == '%')
        return 3;
    if (ch == '*' || ch == '/')
        return 2;
    if (ch == '+' || ch == '-')
        return 1;
    if (ch == ')')
        return 0;
    return -1;
}
int isoperand(char ch)
{
    if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        return 1;
    else
        return 0;
}

/*
//-----------------------OUTPUT--------------------------
// source@beth:~/Desktop/UNI/DS$ gcc infix-postfix-pp.c -o
infix - postfix - pp &&./ infix - postfix - pp
// Enter infix expression: a+b*c-d/e*h
// Postfix Expression: abc*+de/h*-
// source@beth:~/Desktop/UNI/DS$ gcc infix-postfix-pp.c -o
infix -
postfix - pp &&./ infix - postfix - pp
// Enter infix expression: ((a-(b+c))*d)^(e+f)
// Postfix Expression: abc+-d*ef+^
// source@beth:~/Desktop/DS$ gcc infix-postfix.c -o infixpostfix && ./infix-postfix
// source@beth:~/Desktop/UNI/DS$ gcc infix-postfix-pp.c -o
infix -
postfix - pp &&./ infix - postfix - pp
// Enter infix expression: ((a-(b+c)*d)^(e+f)
// Invalid Expression

*/